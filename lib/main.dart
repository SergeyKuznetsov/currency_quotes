import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:currency_quotes/mapper.dart';
import 'package:currency_quotes/valute.dart';
import 'package:currency_quotes/windows1251_codec.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  Future<List<Valute>> getValuteFromXML(BuildContext context) async {
    String xml = await fetchXmlString();
    final decodedXml = win1251.decode(xml);
    final valutes = ValuteMapper.fromXml(decodedXml);
    return valutes;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green[300],
      appBar: AppBar(
        title: Text(
          'Курс валют',
          style: TextStyle(
              fontStyle: FontStyle.italic,
              fontSize: 30,
              color: Colors.green[50]),
        ),
        centerTitle: true,
      ),
      body: FutureBuilder(
          future: getValuteFromXML(context),
          builder: (context, data) {
            if (data.hasData) {
              List<Valute> valute = data.data;

              final valuteNew = [];

              for (var i = 0; i < valute.length; i++) {
                if (valute[i].name == 'Доллар США' ||
                    valute[i].name == 'Евро' ||
                    valute[i].name == 'Белорусский рубль' ||
                    valute[i].name == 'Казахстанских тенге') {
                  valuteNew.add(valute[i]);
                }
              }

              return ListView.builder(
                  itemCount: valuteNew.length,
                  itemBuilder: (context, index) {
                    return Container(
                      height: 175,
                      margin: const EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          border: Border.all(),
                          color: Colors.green[100],
                          borderRadius: BorderRadius.circular(20.0)),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 40,
                          ),
                          Text(
                            valuteNew[index].name,
                            style: TextStyle(
                                fontSize: 30.0,
                                fontWeight: FontWeight.bold,
                                fontStyle: FontStyle.italic,
                                color: Colors.green[800]),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Text(
                            '${valuteNew[index].nominal}' +
                                '  ' +
                                '${valuteNew[index].charCode}' +
                                '    ' +
                                '=' +
                                '    ' +
                                '${valuteNew[index].value}' +
                                '  ' +
                                'RUB',
                            style: TextStyle(
                                fontSize: 25.0,
                                color: Colors.green[900],
                                fontWeight: FontWeight.normal),
                          ),
                        ],
                      ),
                    );
                  });
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
    );
  }
}

Future<String> fetchXmlString() async {
  final url = Uri.parse('https://www.cbr.ru/scripts/XML_daily.asp');
  http.Response response = await http.get(url);

  if (response.statusCode == 200) {
    return response.body;
  }

  throw Exception();
}
