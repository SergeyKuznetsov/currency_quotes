import 'dart:convert';
import 'package:currency_quotes/valute.dart';
import 'package:xml2json/xml2json.dart';

extension ValuteMapper on List<Valute> {
  static List<Valute> fromXml(xml) {
    final map = jsonDecode((Xml2Json()..parse(xml)).toBadgerfish());

    return map['ValCurs']['Valute']
        .map<Valute>((e) => Valute(e['Name']['\$'], e['Value']['\$'],
            e['Nominal']['\$'], e['CharCode']['\$']))
        .toList();
  }
}
