class Valute {
  final String name;
  final String value;
  final String nominal;
  final String charCode;

  Valute(this.name, this.value, this.nominal, this.charCode);
}
